#include <QString>

QString GetRevisionNumber()
{
    return QString::fromLatin1("VERSION").trimmed();
}
