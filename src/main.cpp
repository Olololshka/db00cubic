//-----------------------------------------------------------------
#include <QApplication>
#include <QThread>
#include <QAction>
#include <QString>
#include <QTimer>
#include <QMessageBox>
#include <QSet>
#include <QRegExp>
//-----------------------------------------------------------------
#include <qmodbus/common.h>
#include <qmodbus/Sleeper.h>
#include <qmodbus/ModbusEngine.h>
#include <qmodbus/ModbusRequest.h>
#include <mylog/mylog.h>
//-----------------------------------------------------------------
#include <modbus/modbus.h>
//-----------------------------------------------------------------
#include "Settings.h"
#include "displaygroup.h"
#include "db00_pcubicproxy.h"
#include "SerialDeviceEnumerator.h"
#include "db00arraymodel.h"
//-----------------------------------------------------------------

#define _FT_VID     "0403"
#define _FT_PID     "6001"

struct sDB00Info {
    uint8_t addr;
    uint16_t serial;
    DB00_PcubicProxy::CubicCoeffs coeffs;
};

QList<sDB00Info> getDevicesList()
{
    QList<sDB00Info> l;

    QList<QString> keys = SettingsCmdLine::settings->keys();

    for (QList<QString>::Iterator it = keys.begin();
         it != keys.end(); ++it)
        *it = (*it).split('/').first();

    // http://stackoverflow.com/questions/1571876/stdunique-analogue-in-qt
    QSet<QString> groups = QSet<QString>::fromList(keys);

    QRegExp rx("Sensor.*");

    for (QSet<QString>::Iterator it = groups.begin();
         it != groups.end(); ++it)
        if (rx.indexIn(*it) != -1)
        {
            QString sd = *it;

            sDB00Info info;
            info.addr = SettingsCmdLine::settings->value(sd + "/address").toInt();
            if (info.addr == 0 || info.addr == MODBUS_BROADCAST_ADDRESS)
                continue;

            info.coeffs.P0 = SettingsCmdLine::settings->value(sd + "/P0").toDouble();
            info.coeffs.Fp0 = SettingsCmdLine::settings->value(sd + "/Fp0").toDouble();
            info.coeffs.Ft0 = SettingsCmdLine::settings->value(sd + "/Ft0").toDouble();

            for (int i = 1; i <= 11; ++i)
                info.coeffs.A[i] = SettingsCmdLine::settings->value(
                            sd + "/A" + QString::number(i)).toDouble();

            l.append(info);
        }

    return l;
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName("DB00Cubic");

    //настройко-держатель
    Settings _settings(argc, argv);

    //Лог
    MyLog::myLog _LOG;

#if (_DEBUG_OUTPUT == 1)
    _LOG.setLogLevel((MyLog::enLogLevel)4);
#endif

    LOG_INFO("Reading configuration..");

    QList<sDB00Info> devs = getDevicesList();
    if (devs.empty())
    {
        QMessageBox::critical(NULL, QObject::trUtf8("Не определена конфигурация"),
                              QObject::trUtf8("В конфигурационном файли не обнаружено сведений об устройствах. Выход."));
        LOG_FATAL("No config data avalable");
        return 1;
    }

    // синхронно-асинхронный движок Modbus
    ModbusEngine _ModbusCore;

    SerialDeviceEnumerator* enumerator = SerialDeviceEnumerator::instance();
    QStringList avalable = enumerator->devicesAvailable();
    foreach(QString portname, avalable)
    {
        enumerator->setDeviceName(portname);
        if ((enumerator->vendorID() != _FT_VID) || (enumerator->productID() != _FT_PID) || (enumerator->isBusy() == true))
            avalable.removeOne(portname);
        else
            if (portname.at(0) != '/')
            {
                QString number = portname;
                number.remove(0, 3);
                if (number.toInt() > 9)
                {
                    avalable.removeOne(portname);
                    avalable.append("\\\\.\\" + portname);
                }
            }
    }
    enumerator->deleteLater();

    if (avalable.empty())
    {
        QMessageBox::critical(NULL, QObject::trUtf8("Поддерживаемых устройств не найдено"),
                              QObject::trUtf8("Не найдено виртуальных последовательных портов FTDI.\nУбедитесь, что устройство подключено."));
        return 2;
    }

    QString Port = SettingsCmdLine::settings->value("Global/Port").toString();
    if (Port.isEmpty() || (!avalable.contains(Port)))
        (*SettingsCmdLine::settings)["Global/Port"] = avalable.first();

    _ModbusCore.RestartConnection();
    _ModbusCore.setTryCloseonError(false);

    struct timeval _timeout = {0, 30000};
    modbus_set_response_timeout(_ModbusCore.getModbusContext(), _timeout.tv_sec, _timeout.tv_usec);

    QList<DB00*> devices;
    for (QList<sDB00Info>::ConstIterator it = devs.begin();
         it != devs.end(); ++it)
    {
        DB00 *d = new DB00_PcubicProxy(&_ModbusCore, (*it).addr, (*it).coeffs);
        LOG_INFO(QObject::trUtf8("Testing device address %1 avalable...").arg((*it).addr));
        if (d->test())
        {
            LOG_INFO(QObject::trUtf8("Ok"));
            devices.append(d);

        } else {
            LOG_INFO(QObject::trUtf8("FAIL"));
        }
    }

    if (devices.empty()) {
        QMessageBox::critical(NULL, QObject::trUtf8("Устройств не найдено"),
                              QObject::trUtf8("Тестирование не обнаружило объявленых в конфигурации устройств.\nПроверьте подключение."));
        return 3;
    }

    DB00ArrayModel *model = new DB00ArrayModel(&devices);
    DisplayGroup *mainform = new DisplayGroup(model);

    QObject::connect(&_ModbusCore, SIGNAL(ModbusGeneralFailure(int)), mainform, SLOT(generalModbusfailure()), Qt::QueuedConnection);

    mainform->show();

    int res = app.exec();

    delete mainform;
    delete model;

    _ModbusCore.close();

    for (QList<DB00*>::ConstIterator it = devices.begin();
         it != devices.end(); ++it)
        (*it)->deleteLater();

    return res;
}
//-----------------------------------------------------------------
