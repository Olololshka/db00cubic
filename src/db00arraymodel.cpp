#include <QList>
#include <QTime>
#include <QFont>

#include <settingscmdline/settingscmdline.h>

#include "DB00.h"

#include "db00arraymodel.h"

#define MIN_DELAY_MS    50

DB00ArrayModel::DB00ArrayModel(QList<DB00 *> *devices, QObject *parent) :
    QAbstractTableModel(parent)
{
    this->devices = devices;

    for (int i = 0; i < devices->size(); ++i)
        values[devices->at(i)->getAddres()] = new _data();

    updater = new Updater(this);
    updater->start();

    font = new QFont();
    font->setPixelSize(20);
    font->setBold(true);
}

DB00ArrayModel::~DB00ArrayModel()
{
    delete font;

    mutex.lock();
    updater->stop();
    Updater::msleep(10);
    if (updater->isRunning())
    {
        updater->terminate();
        Updater::msleep(10);
    }
}

int DB00ArrayModel::columnCount(const QModelIndex &parent) const
{
    return 5 - 2;
}

Qt::ItemFlags DB00ArrayModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEnabled;
}

QVariant DB00ArrayModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::FontRole)
        return *font;

    if (role == Qt::DisplayRole)
    {
        uint8_t addr = devices->at(index.row())->getAddres();
        QVariant res;
        mutex.lock();
        switch (index.column())
        {
        case 0:
            res = values[addr]->serial;
            break;
        case 1:
        {
            bool ok;
            int acuracy = SettingsCmdLine::settings->value("Pressure/DisplayAcuracy").toInt(&ok);
            if (ok)
                res = QString::number(values[addr]->fP, 'f', acuracy);
            else
                res = QString::number(values[addr]->fP, 'e');
        }
            break;
        case 2:
            res = QString::number(values[addr]->fT, 'f', 2);
            break;
        case 3:
            res = QString::number(values[addr]->fFp, 'f', 2);
            break;
        case 4:
            res = QString::number(values[addr]->fFt, 'f', 2);
            break;
        }
        mutex.unlock();
        return res;
    }

    if (role == Qt::TextAlignmentRole)
        return Qt::AlignCenter;

    return QVariant();
}

QVariant DB00ArrayModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if ((orientation == Qt::Horizontal) && (role == Qt::DisplayRole))
    { // section - номер колонки
        switch(section)
        {
        case 0: return trUtf8("Серийный номер");
        case 1: return trUtf8("Давление [%1]").arg(
                        SettingsCmdLine::settings->value("Pressure/Units").toString());
        case 2: return trUtf8("Температура [°C]");
        case 3: return trUtf8("Частота давления\n[Гц]");
        case 4: return trUtf8("Частота температуры\n[Гц]");
        default: return QVariant();
        }
    }
    return QAbstractTableModel::headerData(section, orientation, role);
}

int DB00ArrayModel::rowCount(const QModelIndex &parent) const
{
    return devices->size();
}

void DB00ArrayModel::updateRow(int row)
{
    emit dataChanged(index(row, 0), index(row, columnCount() - 1));
}

DB00ArrayModel::Updater::Updater(DB00ArrayModel *parent)
{
    model = parent;
    stopCondition = false;
}

void DB00ArrayModel::Updater::stop()
{
    stopCondition = true;
}

void DB00ArrayModel::Updater::run()
{
    this->setTerminationEnabled(true);

    QTime time = QTime::currentTime();
    //int e = 0;
    for(;;)
    {
        if (stopCondition)
            return;

        QTime ctime = QTime::currentTime();
        if (time.msecsTo(ctime) > MIN_DELAY_MS)
        {
            time = ctime;

            QList<float> v;
            bool ok;
            int row = 0;
            for (QList<DB00 *>::Iterator it = model->devices->begin();
                 it != model->devices->end(); ++it, ++row)
            {
                v = (*it)->getOutputValues(&ok);
                if (ok)
                {
                    DB00ArrayModel::_data *d = model->values[(*it)->getAddres()];

                    if (!d->serial)
                    {
                        model->mutex.lock();
                        d->serial = (*it)->getSerial();
                        model->mutex.unlock();
                    }

                    model->mutex.lock();
                    d->fP = v.at(0);
                    d->fT = v.at(1);
                    d->fFp = v.at(2);
                    d->fFt = v.at(3);
                    model->mutex.unlock();

                    if (stopCondition)
                        return;

                    emit model->updateRow(row);
                }
            }
        } else
            QThread::msleep(1);
    }
}
