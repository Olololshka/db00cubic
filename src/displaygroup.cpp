#include <QAction>
#include <QToolBar>
#include <QApplication>
#include <QMessageBox>
#include <QTableView>
#include <QHeaderView>
#include <QFile>
#include <QByteArray>
#include <QVariantMap>
#include <QTextCodec>

#include <qmodbus/common.h>

#include <mylog/mylog.h>

#include "QDateTimeDelta.h"
#include "RecordSetup.h"
#include "displaygroup.h"

DisplayGroup::DisplayGroup(QAbstractTableModel* model, QWidget *parent) :
    QMainWindow(parent)
{
    outputFile = NULL;
    this->model = model;
    sizeready = false;

    codec = QTextCodec::codecForName("CP1251");

    RecordAction = new QAction(trUtf8("Запись в файл"), this);
    RecordAction->setIcon(QIcon(":/res/media_record.png"));
    RecordAction->setCheckable(true);

    //QToolBar *toolbar = new QToolBar;
    //toolbar->addAction(RecordAction);
    //addToolBar(Qt::TopToolBarArea, toolbar);

    table = new QTableView(this);
    table->setModel(model);
    table->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    setCentralWidget(table);

    table->addAction(RecordAction);
    table->setContextMenuPolicy(Qt::ActionsContextMenu);

    connect(RecordAction, SIGNAL(triggered(bool)), this, SLOT(recordToFile(bool)));
    connect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this,
            SLOT(dataUpdated(QModelIndex,QModelIndex)), Qt::QueuedConnection);
}

void DisplayGroup::closeEvent(QCloseEvent *e)
{
    recordToFile(false);

    QMainWindow::closeEvent(e);
}

void DisplayGroup::generalModbusfailure()
{
    recordToFile(false);
    QMessageBox::critical(this, trUtf8("Соединение разорвано."), trUtf8("Соединение разорвано, кабель был отсоединен?"));
    qApp->exit(3);
}

void DisplayGroup::dataUpdated(const QModelIndex &first, const QModelIndex &)
{
    if (!sizeready)
    {
        sizeready = true;
        _resize();
    }

    if (outputFile != NULL)
    {
        LOG_DEBUG(trUtf8("Writing data"));

        QString Firstcolumn;
        switch (DateMode)
        {
        case 0:
            //Firstcolumn = QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz") + ";";
            Firstcolumn = QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.z") + ";";
            break;
        case 1:
            Firstcolumn = QDateTimeDelta(QDateTime::currentDateTime(), StartTime) + ";";
            break;
        case 2:
            Firstcolumn = QString::number(counter++) + ";";
            break;
        case 3:
            Firstcolumn = "";
            break;
        }

        QString Measures = WriteTemplate;

        const QModelIndex Pidx = model->index(first.row(), 1);
        const QModelIndex Tidx = model->index(first.row(), 2);
        const QModelIndex Fpidx = model->index(first.row(), 3);
        const QModelIndex Ftidx = model->index(first.row(), 4);

        Measures.replace(
                    "%WADDR%", model->data(first).toString()).replace(
                    "%P%", model->data(Pidx).toString()).replace(
                    "%T%", model->data(Tidx).toString()).replace(
                    "%PF%", model->data(Fpidx).toString()).replace(
                    "%TF%", model->data(Ftidx).toString());
#if (_DEBUG_OUTPUT == 1)
        LOG_DEBUG(Firstcolumn + Measures);
#endif
        QByteArray writestring;
        writestring.append(Firstcolumn);
        writestring.append(Measures);
        outputFile->write(writestring);
    }
}

void DisplayGroup::recordToFile(bool startrecord)
{
    if (startrecord)
    {
        RecordSetup setupwindow;
        setupwindow.exec();
        QVariantMap setup = setupwindow.getSetup();
        if (setup.isEmpty())
        {
            RecordAction->setChecked(false);
            return;
        }
#if (_DEBUG_OUTPUT == 1)
        foreach(QString key, setup.keys())
        {
            LOG_INFO(trUtf8("%1 -> %2").arg(key).arg(setup.value(key, "").toString()));
        }
#endif
        outputFile = new QFile(setup.value("filename").toString());
        if (!outputFile->open(QIODevice::WriteOnly | QIODevice::Text))
        {
            LOG_ERROR(trUtf8("Failed to open file %1.").arg(setup.value("filename").toString()));
            __DELETE(outputFile);
            RecordAction->setChecked(false);
        }
        else
        {
            DateMode = (char) setup.value("dateMode").toInt();
            QString firstcolumname;
            switch (DateMode)
            {
            case 0:
                firstcolumname = trUtf8("Дата;");
                break;
            case 1:
                firstcolumname = trUtf8("Таймер;");
                StartTime = QDateTime::currentDateTime();
                break;
            case 2:
                firstcolumname = trUtf8("Счетчик;");
                counter = 0;
                break;
            }
            QString headText = trUtf8("%1%2%3%4%5").arg(
                        trUtf8("Адрес;")).arg(
                        setup.value("WPF").toBool() ? trUtf8("Частота давления [Гц];") : ("")).arg(
                        setup.value("WTF").toBool() ? trUtf8("Частота температуры [Гц];") : ("")).arg(
                        setup.value("WP").toBool() ? trUtf8("Давление;") : ("")).arg(
                        setup.value("WT").toBool() ? trUtf8("Температура;") : (""));
            *(headText.end() - 1) = '\n';


            QByteArray outdata;
            outdata.append(codec->fromUnicode(firstcolumname));
            outdata.append(codec->fromUnicode(headText));
            outputFile->write(outdata);

            WriteTemplate = trUtf8("%1%2%3%4%5").arg(
                        "%WADDR%;").arg(
                        setup.value("WPF").toBool() ? ("%PF%;") : ("")).arg(
                        setup.value("WTF").toBool() ? ("%TF%;") : ("")).arg(
                        setup.value("WP").toBool() ? ("%P%;") : ("")).arg(
                        setup.value("WT").toBool() ? ("%T%;") : (""));
            *(WriteTemplate.end() - 1) = '\n';

            RecordAction->setIcon(QIcon(":/res/media_stop.png"));
            RecordAction->setText(trUtf8("Остановить запись"));
            LOG_INFO(trUtf8("Start recording to file %1").arg(setup.value("filename").toString()));
        }
    }
    else
    {
        if (outputFile != NULL)
        {
            outputFile->close();
            __DELETE(outputFile);
            LOG_INFO(trUtf8("Record stoped"));
        }
        RecordAction->setIcon(QIcon(":/res/media_record.png"));
        RecordAction->setText(trUtf8("Запись в файл"));
    }
}

void DisplayGroup::_resize()
{
    resize(table->horizontalHeader()->length()+20, table->verticalHeader()->length());
}
