#ifndef DB00ARRAYMODEL_H
#define DB00ARRAYMODEL_H

#include <QAbstractTableModel>
#include <QThread>
#include <QMap>
#include <QMutex>

#include <stdint.h>

class DB00;
class QFont;

class DB00ArrayModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    friend class Updater;

    class _data {
    public:
        uint16_t serial;
        float fP, fT, fFp, fFt;
    };

    DB00ArrayModel(QList<DB00 *> *devices, QObject *parent = 0);
    ~DB00ArrayModel();
    
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    void updateRow(int row);

private:
    QList<DB00 *> *devices;

    QMap<uint8_t, _data*> values;

    mutable QMutex mutex;

    class Updater : public QThread
    {
    public:
        Updater(DB00ArrayModel* parent);

        void stop();
        static void msleep(unsigned long ms) { QThread::msleep(ms); }

    protected:
        void run();

    private:
        DB00ArrayModel* model;

        bool stopCondition;
    };

    Updater * updater;

    QFont *font;
};

#endif // DB00ARRAYMODEL_H
