#ifndef DB00_PCUBICPROXY_H
#define DB00_PCUBICPROXY_H

#include "DB00.h"

class DB00_PcubicProxy : public DB00
{
    Q_OBJECT
public:
    class CubicCoeffs {
    public:
        double P0, A[11 + 1], Fp0, Ft0;
    };

    DB00_PcubicProxy(ModbusEngine* mb, unsigned char adress, QObject* parent = NULL);
    DB00_PcubicProxy(ModbusEngine* mb, unsigned char adress, const CubicCoeffs &coeffs, QObject* parent = NULL);

    QList<float> getOutputValues(bool *ok = NULL);

private:
    CubicCoeffs coeffs;
};

#endif // DB00_PCUBICPROXY_H
