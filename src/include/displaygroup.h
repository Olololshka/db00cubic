#ifndef DISPLAYGROUP_H
#define DISPLAYGROUP_H

#include <QMainWindow>
#include <QDateTime>

class DB00;
class RecordAcrion;
class QAbstractTableModel;
class QFile;
class QModelIndex;
class QTextCodec;
class QTableView;

class DisplayGroup : public QMainWindow
{
    Q_OBJECT
public:
    DisplayGroup(QAbstractTableModel* devices, QWidget *parent = NULL);

protected:
    void closeEvent(QCloseEvent *e);

private slots:
    void generalModbusfailure();
    void dataUpdated(const QModelIndex &first, const QModelIndex&);

public slots:
    void recordToFile(bool startrecord);

private:
    QAction *RecordAction;

    QFile* outputFile;

    char DateMode;
    int counter;

    QDateTime StartTime;

    QString WriteTemplate;

    QAbstractTableModel* model;

    QTextCodec *codec;

    bool sizeready;

    QTableView *table;

    void _resize();
};

#endif // DISPLAYGROUP_H
