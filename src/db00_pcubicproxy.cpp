#include <QVariant>

#include "Settings.h"

#include "db00_pcubicproxy.h"


DB00_PcubicProxy::DB00_PcubicProxy(ModbusEngine *mb, unsigned char adress, QObject *parent) :
    DB00(mb, adress, parent)
{
    // load coeffs
    coeffs.P0 = SettingsCmdLine::settings->value("Pressure/P0").toDouble();
    coeffs.Fp0 = SettingsCmdLine::settings->value("Pressure/Fp0").toDouble();
    coeffs.Ft0 = SettingsCmdLine::settings->value("Pressure/Ft0").toDouble();

    for (int i = 1; i <= 11; ++i) {
        coeffs.A[i] = SettingsCmdLine::settings->value("Pressure/A"+QString::number(i)).toDouble();
    }
}

DB00_PcubicProxy::DB00_PcubicProxy(ModbusEngine *mb, unsigned char adress, const CubicCoeffs &coeffs, QObject *parent) :
    DB00(mb, adress, parent)
{
    this->coeffs = coeffs;
}

QList<float> DB00_PcubicProxy::getOutputValues(bool *ok)
{
    bool _ok;
    if (!ok)
        ok = &_ok;

    QList<float> d = DB00::getOutputValues(ok);

    if (*ok)
    {
        double B = d.at(2) - coeffs.Fp0;
        double C = d.at(3) - coeffs.Ft0;
        double C2 = C * C;
        double B2 = B * B;
        double B3 = B2 * B;

        double P = coeffs.P0 +
                coeffs.A[1] * C +
                coeffs.A[2] * C2 +
                coeffs.A[3] * B +
                coeffs.A[4] * B2 +
                coeffs.A[5] * C * B +
                coeffs.A[6] * C * B2 +
                coeffs.A[7] * C2 * B +
                coeffs.A[8] * C2 * B2 +
                coeffs.A[9] * B3 +
                coeffs.A[10] * C * B3 +
                coeffs.A[11] * C2 * B3;

        d[0] = (float)P;
    }
    return d;
}
